var trainig = angular.module('training', ['panel']);
trainig.controller('userCtrl', ['$scope' , function ($scope) {
    this.pageTitle = "Pop-up title!";

    this.currentTabId = 1;

    this.panelsList = [{
        title:"First",
        id:1
    },{
        title:"Second",
        id:2
    },{
        title:"Third",
        id:3
    }];

}]);
trainig.service('contentService',function () {
    this.tabContent = [{
        id:1,
        title:"Content 1",
        content:"Directives are complicated. Directives that need to collaborate are even more complicated. Having an " +
        "understanding of the tools Angular provides in this scenario can help you manage and contain this complexity " +
        "within your application."
    },{
        id:2,
        title:"Content 2",
        content:"This is a mind-blowing read and causes a new way of thinking about angular directives – thank you " +
        "for posting it and being so clear and concise."
    },{
        id:3,
        title:"Content 3",
        content:" much prefer your third example and the use of controller over broadcasting events and using scope " +
        "inheritance. However, doesn’t this approach result in child directives that are tightly coupled to their " +
        "parent directive? E.g in your above example, annotatedImageCurrent, annotatedImageViewer, and annotatedImage" +
        "Controls can only be used with the annotatedImage directive."
    }];
});


var panel = angular.module('panel', []);

panel.directive('tabControl',['contentService', function (contentService) {
    return {
        scope:{
            currentId:"="
        },
        bindToController: {
            currentId:"="
        },
        controllerAs: 'ctrl',
        transclude:true,
        templateUrl:'panel_wrap.html',
        controller: function($scope, $element){
            var _this = this;

            this.currentId = $scope.currentId;

            this.content = {
                title:'',
                text:''
            };

            this.setCurrent = function (id) {
                if(id){ //set new
                    this.currentId = id;
                }else{ // else read current
                    return this.currentId;
                }
            };

            this.loadContent = function (id) {
               id = id || _this.setCurrent();

               var content = contentService.tabContent.filter(function (value) {
                   return value.id == id;
               });
                this.setCurrent(id);
               _this.content.text = content[0].content;
               _this.content.title = content[0].title;
            };

            this.loadContent();

        }
    }
}]);
panel.directive('pane', function () {
    return {
        transclude:true,
        require:"^tabControl",
        scope:{
            paneObj:"=",
        },
        templateUrl:"pane.html",
        link:function (scope, element, attr, tabControl) {

            scope.currentId = tabControl.setCurrent();

            scope.loadContentHandler = function (id) {
                tabControl.loadContent(id);
                scope.currentId = tabControl.setCurrent();
            };
        }
    }
});





