'use strict';


var gulp = require('gulp'),
    inject = require('gulp-inject'),
    watch = require('gulp-watch'),
    mainBowerFiles = require('main-bower-files'),
    es = require('event-stream'),
    minifyCSS = require('gulp-minify-css'),
    sourcemaps   = require('gulp-sourcemaps'),
    postcss      = require('gulp-postcss'),
    autoprefixer = require('autoprefixer'),
    minifyjs = require('gulp-js-minify'),
    sass = require('gulp-sass');

var paths = {
    work: 'var/',
    build: 'build/',

    workJs:  'var/js/',
    workCss: 'var/scss/*.scss'
};





gulp.task('inject', function () {

    var cssFiles = gulp.src(paths.workCss)
        .pipe(autoprefixer())
/*
        .pipe(sass({
            includePaths: require('node-bourbon').includePaths
        }))
*/
        .pipe(gulp.dest(paths.build+'css/'));

    gulp.src('./build/index.html')
        .pipe(inject(gulp.src(mainBowerFiles(), {read: false}), {name: 'bower', relative: true}))
        .pipe(inject(es.merge(
            cssFiles,
            gulp.src('./build/js/*.js', {read: false})
        )))
        .pipe(gulp.dest('../build'));


});


gulp.task('copy-index', function() {

    var cssFiles = gulp.src("build/css/*.css");


    gulp.src([ paths.work + 'index.html',
               paths.work + '*.html',
               paths.work + '*view/**/*.html',
            ])
        .pipe(inject(gulp.src(mainBowerFiles(), {read: false}), {name: 'bower', relative: true}))

        .pipe(inject(gulp.src('build/css/*.css', {read: false}), {relative: true}))
        .pipe(inject(gulp.src(['build/js/*.js','build/js/client/*.js'], {read: false}), {relative: true}))

        .pipe(gulp.dest(paths.build));
});

gulp.task('minify-js', function(){
    gulp.src(paths.workJs + '**/*.js')
        // .pipe(minifyjs())
        .pipe(gulp.dest(paths.build + "js/"));
});



gulp.task('sass', function () {
  gulp.src(paths.workCss)
      .pipe(sourcemaps.init())
      .pipe(postcss([ autoprefixer({ browsers: ['last 2 versions'] }) ]))
/*
      .pipe(sass({
            includePaths: require('node-bourbon').includePaths
      }))
*/
      .pipe(sass().on('error', sass.logError))
      .pipe(minifyCSS())
      .pipe(gulp.dest(paths.build+'css/'));
});




gulp.task("default",['sass','copy-index','minify-js'],function(){
    gulp.watch([paths.workCss], ['sass']);
    gulp.watch([paths.work + '*.html', paths.work + 'view/**/*.html', paths.work + 'view/*.html' ], ['copy-index']);
    gulp.watch([paths.workJs + '**/*.js', paths.workJs + '/*.js'], ['minify-js']);
});